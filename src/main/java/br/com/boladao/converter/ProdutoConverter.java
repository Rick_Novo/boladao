/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.converter;

import br.com.boladao.modelos.Produto;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import br.com.boladao.DAO.ProdutoDAO ; 
import javax.faces.application.FacesMessage;
import javax.faces.convert.ConverterException;

/**
 *
 * @author sala303b
 */
@FacesConverter("produtoConverter")
public class ProdutoConverter implements Converter {

    private ProdutoDAO  dao  = new ProdutoDAO();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                Produto produto = dao.find(Integer.parseInt(value));
                return produto;
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Produto) object).getId());
        } else {
            return null;
        }
    }
}


