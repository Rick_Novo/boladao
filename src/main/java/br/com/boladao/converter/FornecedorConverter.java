/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.converter;

import br.com.boladao.modelos.Fornecedor;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import br.com.boladao.DAO.FornecedorDAO ; 
import javax.faces.application.FacesMessage;
import javax.faces.convert.ConverterException;

/**
 *
 * @author sala303b
 */
@FacesConverter("fornecedorConverter")
public class FornecedorConverter implements Converter {

    private FornecedorDAO  dao  = new FornecedorDAO();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                Fornecedor fornecedor = dao.find(Integer.parseInt(value));
                return fornecedor;
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Fornecedor) object).getId());
        } else {
            return null;
        }
    }
}


