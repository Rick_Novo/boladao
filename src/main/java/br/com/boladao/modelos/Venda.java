/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author sala303b
 */
@Entity
public class Venda extends Entidade{
    
    @ManyToOne 
    private Cliente cliente;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDia;
    @OneToMany(targetEntity = ItemDeVenda.class , cascade = CascadeType.PERSIST)
    private List<ItemDeVenda> listaProdutos ; 

    public Venda() {
        this.dataDia =  new Date() ; 
        this.cliente = new Cliente();
        this.listaProdutos = new ArrayList<>();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getData() {
        return dataDia;
    }
    
 
    public List<ItemDeVenda> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<ItemDeVenda> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
    
    public void AdicionaItem(ItemDeVenda item ){
        this.listaProdutos.add(item);
    }
    
    
    public void RemoveItem(ItemDeVenda item){
        this.listaProdutos.remove(item);
    }
    
    
    public float getTotal(){
        float total = 0 ; 
        
        for (ItemDeVenda item : this.listaProdutos){
            total+=item.getTotal() ;
            
        }
        
        return total;
    }
    
    
    
    

    
}
