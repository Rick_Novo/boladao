/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.modelos;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author sala303b
 */
@Entity
public class ItemDeVenda extends Entidade {

    @ManyToOne
    private Produto produto;
    private int quantidade;
    private float preco;

    public ItemDeVenda() {
        this.produto = new Produto();
        
    }
    
    

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    public String getNomeProduto(){
        return this.getProduto().getNome();
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int Quantidade) {
        this.quantidade = Quantidade;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }
    
    public float getTotal(){
        return this.preco * quantidade  ; 
    }

}
