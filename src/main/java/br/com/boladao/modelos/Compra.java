/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author sala303b
 */
@Entity
public class Compra extends Entidade{
    
    @ManyToOne 
    private Fornecedor fornecedor;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDia;
    @OneToMany(targetEntity = ItemDeCompra.class , cascade = CascadeType.PERSIST)
    private List<ItemDeCompra> listaProdutos ; 

    public Compra() {
        this.dataDia =  new Date() ; 
        this.fornecedor = new Fornecedor();
        this.listaProdutos = new ArrayList<>();
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Date getData() {
        return dataDia;
    }
    
    public List<ItemDeCompra> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<ItemDeCompra> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
    
    public void AdicionaItem(ItemDeCompra item ){
        this.listaProdutos.add(item);
    }
    
    
    public void RemoveItem(ItemDeCompra item){
        this.listaProdutos.remove(item);
    }
    
    
    public float getTotal(){
        float total = 0 ; 
        
        for (ItemDeCompra item : this.listaProdutos){
            total+=item.getTotal() ;
            
        }
        
        return total;
    }
    
    
    
    

    
}
