/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author Roger
 */
@Entity
public class Cliente extends Entidade {

    private String nome;
    private String email;
    private String telefone;
    private String cpf;
    private String rg;
    private String celular;
    
    private String bairro;
    private String rua;
    private String cidade;
    private String cep;
    private String referencia;
    private String uf;
    private int numero;
    
    
    
    public Cliente(String nome, String email, String telefone,String cpf, String rg,  String celular, String bairro, String rua, String cidade, String cep, String referencia, String uf, int numero) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        
        this.celular = celular;
        this.bairro = bairro;
        this.rua = rua;
        this.cidade = cidade;
        this.cep = cep;
        this.referencia = referencia;
        this.uf = uf;
        this.numero = numero;
        this.rg = rg;
        this.cpf = cpf;
    }
    
    
    
    @Override
    public String toString() {
        return nome + email + telefone + celular;
    }
    public Cliente() {

    }

    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    
}
