/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.modelos;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author sala303b
 */
@Entity
public class Produto extends Entidade {

    private float preco;
    private String nome;
    private String unidade;

    public Produto() {
       
    }
    
    

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

}