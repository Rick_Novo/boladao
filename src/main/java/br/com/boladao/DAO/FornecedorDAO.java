/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.DAO;

import br.com.boladao.modelos.Fornecedor;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author sala303b
 */
public class FornecedorDAO extends DAO<Fornecedor>{
    
    public FornecedorDAO() {
        super(Fornecedor.class);
    }   
    
        public  Fornecedor achaFornecedor (Fornecedor fornecedor) {

        Fornecedor fornecedor1 = fornecedor;
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Fornecedor c where 1=1 ");

            if (fornecedor.getNome() != null) {
                sb.append(" and c.id like :arg1");
            }

            Query query = em.createQuery(sb.toString());

            if (fornecedor.getNome() != null) {
                query.setParameter("arg1", "%" + fornecedor.getId() + "%");
            }

            fornecedor = (Fornecedor) query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return fornecedor1;

    }
    
    
     public List<Fornecedor> find(Fornecedor fornecedor) {
        List<Fornecedor> listaFornecedor = new ArrayList<>();
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Fornecedor f where f.ativo = :ativo  ");

            if (fornecedor.getNome() != null) {
                sb.append(" and f.nome like :arg1");
            }
            
             if (fornecedor.getEmail()!= null) {
                sb.append(" and f.email like :arg2");
            }
                      
             if (fornecedor.getTelefone()!= null) {
                sb.append(" and f.telefone like :arg5");
            }
             
            if (fornecedor.getCelular()!= null) {
                sb.append(" and f.celular like :arg6");
            }

       
                     
            
            Query query = em.createQuery(sb.toString());

            query.setParameter("ativo", true);
            
            if (fornecedor.getNome() != null) {
                query.setParameter("arg1", "%" + fornecedor.getNome() + "%");
            }
            
            if (fornecedor.getEmail()!= null) {
                query.setParameter("arg2", "%" + fornecedor.getEmail()+ "%");
            }
            
            if (fornecedor.getTelefone()!= null) {
                query.setParameter("arg5", "%" + fornecedor.getTelefone()+ "%");
            }
            
            if (fornecedor.getCelular()!= null) {
                query.setParameter("arg6", "%" + fornecedor.getCelular()+ "%");
            }
       
            
            
            listaFornecedor = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaFornecedor;
}
     
    public List<Fornecedor> find(String nome) {
        List<Fornecedor> listaFornecedor = new ArrayList<>();
        try {
            this.beginTransaction();

            String jpql = "from Fornecedor c where c.nome like :arg1 ";

            Query query = em.createQuery(jpql);

            if (nome != null && !nome.equals("")) {
                query.setParameter("arg1", nome + "%");
            }

            listaFornecedor = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaFornecedor;
    }

}