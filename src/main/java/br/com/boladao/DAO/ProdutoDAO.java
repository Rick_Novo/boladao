/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.DAO;

import br.com.boladao.modelos.Produto;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author sala303b
 */
public class ProdutoDAO extends DAO<Produto> {

    public ProdutoDAO() {
        super(Produto.class);

        
    }

//    public List<Produto> find(String nome) {
//
//        List<Produto> lista = null;
//
//        try {
//            this.beginTransaction();
//
//            StringBuilder sb = new StringBuilder("select p from Produto p  where 1=1 ");
//
//          
//            
//            if (nome != null && !nome.equals("")) {
//                sb.append("and p.nome like :nome ");
//            }
//
//          
//
//            TypedQuery<Produto> query = em.createQuery(sb.toString(),Produto.class);
//
//            if (nome != null && !nome.equals("")) {
//                query.setParameter("nome", "%" + nome + "%");
//
//            }
//
//            
//
//            lista = query.getResultList();
//            this.closeTransaction();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return lista;
//
//    }
    
    public List<Produto> ListaTodosProdutos() {

        List<Produto> lista = null;

        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("select * from Produto p where 1=1 ");

            Query query = em.createQuery(sb.toString());

            lista = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lista;

    }
    
    public List<Produto> find(String nome) {
        List<Produto> listaProduto = new ArrayList<>();
        try {
            this.beginTransaction();

            String jpql = "from Produto c where c.nome like :arg1 ";

            Query query = em.createQuery(jpql);

            if (nome != null && !nome.equals("")) {
                query.setParameter("arg1", nome + "%");
            }

            listaProduto = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaProduto;
    }

}
