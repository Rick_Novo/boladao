/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.DAO;

import br.com.boladao.modelos.ItemDeVenda;
import br.com.boladao.modelos.Venda;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.transaction.Transaction;

/**
 *
 * @author sala303b
 */
public class VendaDAO extends DAO<Venda> {

    public VendaDAO() {
        super(Venda.class);
    }

    public Venda achaItemDeVenda(Venda venda) {

        Venda venda1 = venda;
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Venda v where 1=1 ");

            if (venda1.getCliente() != null) {
                sb.append(" and i.cliente like :cliente");
            }
            if (venda1 != null) {
                sb.append(" and i.produto like :venda");
            }

            Query query = em.createQuery(sb.toString());

            if (venda1.getCliente() != null) {
                query.setParameter("cliente", "%" + venda1.getCliente() + "%");
            }
            if (venda1.getListaProdutos() != null) {
                query.setParameter("produto", "%" + venda1.getListaProdutos() + "%");
            }

            venda1 = (Venda) query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return venda1;

    }

    /*
     public List<Venda> find(Venda venda) {
     List<Venda> listadevenda = new ArrayList<>();
     try {
     this.beginTransaction();

     StringBuilder sb = new StringBuilder("from ItemDeVenda i where 1=1 ");

     if (itemdevenda.getNome() != null) {
     sb.append(" and f.nome like :arg1");
     }
     if (!itemdevenda.getCnpj().equals("")) {
     sb.append(" and f.cnpj = :cnpj");
     }

     Query query = em.createQuery(sb.toString());

     if (itemdevenda.getNome() != null) {
     query.setParameter("arg1", "%" + itemdevenda.getNome() + "%");
     }

     listaItemDeVenda = query.getResultList();
     this.closeTransaction();
     } catch (Exception ex) {
     ex.printStackTrace();
     }
     return listaItemDeVenda;

     }
     */
    public void salva(Venda venda) {
        em.getTransaction().begin();
        em.persist(venda);
        em.getTransaction().commit();
        em.close();
    }
    
    public List<Venda> vendaPorData(Date dataInicial, Date dataFinal) {

      List<Venda> listaVenda = new ArrayList<>();
      
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder();
            sb.append("from Venda v where 1=1 and v.ativo = :ativo");

            
            if (dataInicial != null) {
                sb.append(" and v.dataDia >= :dataInicio and v.dataDia <= :dataFinal ");

            }

            Query query = em.createQuery(sb.toString());

            if (dataInicial != null) {
                query.setParameter("dataInicio", dataInicial);
                query.setParameter("dataFinal", dataFinal);
            }

            query.setParameter("ativo", true);

            listaVenda = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return listaVenda;

    }


    
}
