/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.DAO;

import br.com.boladao.modelos.Cliente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author sala303b
 */
public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO() {
        super(Cliente.class);
    }   
    
        public  Cliente achaCliente (Cliente cliente) {

        Cliente cliente1 = cliente;
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Cliente c where 1=1 ");

            if (cliente.getNome() != null) {
                sb.append(" and c.id like :arg1");
            }

            Query query = em.createQuery(sb.toString());

            if (cliente.getNome() != null) {
                query.setParameter("arg1", "%" + cliente.getId() + "%");
            }

            cliente = (Cliente) query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cliente1;

    }
    
    
     public List<Cliente> find(Cliente cliente) {
        List<Cliente> listaCliente = new ArrayList<>();
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Cliente c where c.ativo = :ativo  ");

            if (cliente.getNome() != null) {
                sb.append(" and c.nome like :arg1");
            }
            
             if (cliente.getEmail()!= null) {
                sb.append(" and c.email like :arg2");
            }
                      
             if (cliente.getTelefone()!= null) {
                sb.append(" and c.telefone like :arg5");
            }
             
            if (cliente.getCelular()!= null) {
                sb.append(" and c.celular like :arg6");
            }

             if (cliente.getCpf()!= null) {
                sb.append(" and c.cpf like :arg7");
            }
            
            if (cliente.getRg()!= null) {
                sb.append(" and c.rg like :arg8");
            }
                     
            
            Query query = em.createQuery(sb.toString());

            query.setParameter("ativo", true);
            
            if (cliente.getNome() != null) {
                query.setParameter("arg1", "%" + cliente.getNome() + "%");
            }
            
            if (cliente.getEmail()!= null) {
                query.setParameter("arg2", "%" + cliente.getEmail()+ "%");
            }
            
            if (cliente.getTelefone()!= null) {
                query.setParameter("arg5", "%" + cliente.getTelefone()+ "%");
            }
            
            if (cliente.getCelular()!= null) {
                query.setParameter("arg6", "%" + cliente.getCelular()+ "%");
            }
            
            if (cliente.getCpf()!= null) {
                query.setParameter("arg7", "%" + cliente.getCpf()+ "%");
            }
            
            if (cliente.getRg()!= null) {
                query.setParameter("arg8", "%" + cliente.getRg()+ "%");
            }
            
            
            listaCliente = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaCliente;
}
     
    public List<Cliente> find(String nome) {
        List<Cliente> listaCliente = new ArrayList<>();
        try {
            this.beginTransaction();

            String jpql = "from Cliente c where c.nome like :arg1 ";

            Query query = em.createQuery(jpql);

            if (nome != null && !nome.equals("")) {
                query.setParameter("arg1", nome + "%");
            }

            listaCliente = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaCliente;
    }

}