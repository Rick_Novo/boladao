/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.DAO;

import br.com.boladao.modelos.ItemDeCompra;
import br.com.boladao.modelos.Compra;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.transaction.Transaction;

/**
 *
 * @author sala303b
 */
public class CompraDAO extends DAO<Compra> {

    public CompraDAO() {
        super(Compra.class);
    }

    public Compra achaItemDeCompra(Compra compra) {

        Compra compra1 = compra;
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder("from Compra v where 1=1 ");

            if (compra1.getFornecedor() != null) {
                sb.append(" and i.fornecedor like :fornecedor");
            }
            if (compra1 != null) {
                sb.append(" and i.produto like :compra");
            }

            Query query = em.createQuery(sb.toString());

            if (compra1.getFornecedor() != null) {
                query.setParameter("fornecedor", "%" + compra1.getFornecedor() + "%");
            }
            if (compra1.getListaProdutos() != null) {
                query.setParameter("produto", "%" + compra1.getListaProdutos() + "%");
            }

            compra1 = (Compra) query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return compra1;

    }

    /*
     public List<Compra> find(Compra compra) {
     List<Compra> listadecompra = new ArrayList<>();
     try {
     this.beginTransaction();

     StringBuilder sb = new StringBuilder("from ItemDeCompra i where 1=1 ");

     if (itemdecompra.getNome() != null) {
     sb.append(" and f.nome like :arg1");
     }
     if (!itemdecompra.getCnpj().equals("")) {
     sb.append(" and f.cnpj = :cnpj");
     }

     Query query = em.createQuery(sb.toString());

     if (itemdecompra.getNome() != null) {
     query.setParameter("arg1", "%" + itemdecompra.getNome() + "%");
     }

     listaItemDeCompra = query.getResultList();
     this.closeTransaction();
     } catch (Exception ex) {
     ex.printStackTrace();
     }
     return listaItemDeCompra;

     }
     */
    public void salva(Compra compra) {
        em.getTransaction().begin();
        em.persist(compra);
        em.getTransaction().commit();
        em.close();
    }
    
    
     public List<Compra> compraPorData(Date dataInicial, Date dataFinal) {

      List<Compra> listaCompra = new ArrayList<>();
      
        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder();
            sb.append("from Compra c where 1=1 and c.ativo = :ativo");

            
            if (dataInicial != null) {
                sb.append(" and c.dataDia >= :dataInicio and c.dataDia <= :dataFinal ");

            }

            Query query = em.createQuery(sb.toString());

            if (dataInicial != null) {
                query.setParameter("dataInicio", dataInicial);
                query.setParameter("dataFinal", dataFinal);
            }

            query.setParameter("ativo", true);

            listaCompra = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return listaCompra;

    }
}
