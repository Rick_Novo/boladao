/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.CompraDAO;
import br.com.boladao.modelos.Compra;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Daniel
 */
@ManagedBean
public class PesquisaComprasBean extends Bean {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");

    private final CompraDAO dao = new CompraDAO();
    private Compra compra = new Compra();

    private Date dataInicial;
    private Date dataFinal;
    
      private List<Compra> listaCompra;
      
    public List<Compra> getListaCompra() {
        return listaCompra;
    }

    public void setListaCompra(List<Compra> listaCompra) {
        this.listaCompra = listaCompra;
    }

  

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(compra);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso!");
            this.compra = new Compra();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(compra.getId(), Compra.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public String editar() {

        System.out.println("a");

        return null;

    }

    public List<Compra> pesquisaPorData() {
        

        listaCompra = dao.compraPorData(dataInicial, dataFinal);

        return listaCompra;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

   public void setDataFim(Date dataFim) {

        Calendar c = Calendar.getInstance();

        if (dataFim != null) {
            c.setTime(dataFim);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);

            this.dataFinal = c.getTime();
        } else if (this.dataInicial != null) {
            c.setTime(this.dataInicial);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);

            this.dataFinal = c.getTime();
        } else {
            this.dataFinal = dataFim;
        }
}
}
