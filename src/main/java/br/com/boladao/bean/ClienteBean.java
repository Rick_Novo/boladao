/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.ClienteDAO;
import br.com.boladao.modelos.Cliente;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 *
 * @author Daniel
 */
@ManagedBean(name = "ClienteBean")
public class ClienteBean extends Bean {

     private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");
     
    private final ClienteDAO dao = new ClienteDAO();

    private Cliente cliente = new Cliente();
   

    private List<Cliente> listaCliente;

    public ClienteBean() {
        this.pesquisaClientes();
    }

    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(cliente);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso!");
            this.cliente = new Cliente();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(cliente.getId(), Cliente.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }
    
   public String editar(Cliente c) {

       try {
            dao.beginTransaction();
            dao.update(c);
            dao.commitAndCloseTransaction();
            addMessageAviso("Atualizado com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
        return null;

    }

    public List<Cliente> getClientes() {
        List<Cliente> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.closeTransaction();
        return lista;
    }
    
      public List<Cliente> encontreiro(String a) {
        List<Cliente> lista;

        lista = dao.find(a);

        return lista;
    }
    

    public void pesquisaClientes() {
       this.listaCliente = dao.find(this.cliente);
       
    }

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public ClienteDAO getDao() {
        return dao;
    }
}
