/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.FornecedorDAO;
import br.com.boladao.modelos.Fornecedor;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 *
 * @author Daniel
 */
@ManagedBean(name = "FornecedorBean")
public class FornecedorBean extends Bean {

     private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");
     
    private final FornecedorDAO dao = new FornecedorDAO();

    private Fornecedor fornecedor = new Fornecedor();
   

    private List<Fornecedor> listaFornecedor;

    public FornecedorBean() {
        this.pesquisaFornecedors();
    }

    
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(fornecedor);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso!");
            this.fornecedor = new Fornecedor();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(fornecedor.getId(), Fornecedor.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }
    
   public String editar(Fornecedor c) {

       try {
            dao.beginTransaction();
            dao.update(c);
            dao.commitAndCloseTransaction();
            addMessageAviso("Atualizado com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
        return null;

    }

    public List<Fornecedor> getFornecedors() {
        List<Fornecedor> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.closeTransaction();
        return lista;
    }
    
      public List<Fornecedor> encontreiro(String a) {
        List<Fornecedor> lista;

        lista = dao.find(a);

        return lista;
    }
    

    public void pesquisaFornecedors() {
       this.listaFornecedor = dao.find(this.fornecedor);
       
    }

    public List<Fornecedor> getListaFornecedor() {
        return listaFornecedor;
    }

    public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
        this.listaFornecedor = listaFornecedor;
    }

    public FornecedorDAO getDao() {
        return dao;
    }
}
