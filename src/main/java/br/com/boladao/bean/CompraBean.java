/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.FornecedorDAO;
import br.com.boladao.DAO.CompraDAO;
import br.com.boladao.modelos.Fornecedor;
import br.com.boladao.modelos.ItemDeCompra;
import br.com.boladao.modelos.Produto;
import br.com.boladao.modelos.Compra;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author sala303b
 */
@ManagedBean(name = "CompraBean")
@SessionScoped
public class CompraBean extends Bean {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");

    private Produto produtoSelecionado;

    private final CompraDAO dao = new CompraDAO();

    private Compra compra = new Compra();

    private int quantidadeProduto;

    private float preco;

    public CompraBean() {

    }

    public void salvarItem() {

        try {
            dao.beginTransaction();
            dao.save(compra);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso !");
            this.compra = new Compra();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

    }

            public Fornecedor getFornecedor() {
        return this.compra.getFornecedor();
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.compra.setFornecedor(fornecedor);
    }

    private boolean validarProduto() {

       
        if (quantidadeProduto == 0) {
            addMessageAviso("Favor selecionar a quantidade!");
            return false;
        }

        if (preco == 0) {
            addMessageAviso("Favor selecionar valor!");
            return false;
        }
        return true;
    }

    public void adicionarProduto() {

        if (this.validarProduto()) {
            ItemDeCompra item = new ItemDeCompra();
            item.setProduto(produtoSelecionado);
            item.setPreco(preco);
            item.setQuantidade(quantidadeProduto);

            this.compra.getListaProdutos().add(item);

            this.produtoSelecionado = new Produto();

            this.quantidadeProduto = 0;
            this.preco = 0;
        }

//        try{
//        dao.beginTransaction();
//            dao.save(compra);
//            dao.commitAndCloseTransaction();
//            addMessageInfo("Salvo com Sucesso!");
//            this.compra = new Compra();
//        } catch (Exception ex) {
//            dao.rollback();
//            addMessageErro(ex.getMessage());
//        }
    }

    public List<ItemDeCompra> getItems() {
        return this.compra.getListaProdutos();

    }

    public int getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(int quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public void atualizarPreco(SelectEvent e) {

        this.preco = this.produtoSelecionado.getPreco();

    }

    /* public List<Fornecedor> getFornecedors() {
     List<Fornecedor> lista;
     dao.beginTransaction();
     lista = dao.findAll();
     dao.closeTransaction();
     return lista;
     } */
    public void salvar() {

        try {
            dao.salva(compra);
            addMessageInfo("Compra No." + compra.getId() + " Salva com Sucesso !");
            this.compra = new Compra();
        } catch (RuntimeException re) {
            re.printStackTrace();
            dao.rollback();
            throw re;
        } catch (Exception ex) {
            ex.printStackTrace();
            addMessageErro(ex.getMessage());
        }
    }

    public int getMaxSpiner() {
        return quantidadeProduto;
    }

    public void setMaxSpiner(int maxSpiner) {
        this.quantidadeProduto = maxSpiner;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

}
