/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.ClienteDAO;
import br.com.boladao.modelos.Cliente;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roger
 */
@ManagedBean
@ViewScoped
public class PesquisaClienteBean extends Bean {

    private final ClienteDAO dao = new ClienteDAO();

    private Cliente clienteSelecionado = new Cliente();
    private Cliente clientePesquisado = new Cliente();

    
    public Cliente getClienteSelecionado() {
        return clienteSelecionado;
    }

    public void setClienteSelecionado(Cliente clienteSelecionado) {
        this.clienteSelecionado = clienteSelecionado;
    }

    public Cliente getClientePesquisado() {
        return clientePesquisado;
    }

    public void setClientePesquisado(Cliente clientePesquisado) {
        this.clientePesquisado = clientePesquisado;
    }

    private List<Cliente> listaCliente;

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }
    
  
  

    public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(this.clienteSelecionado);
            dao.commitAndCloseTransaction();
            addMessageAviso("Alterado com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
        return null;

    }

    public void setarfalso() {

        try {
            dao.beginTransaction();
            this.clienteSelecionado.setAtivo(false);
            dao.update(clienteSelecionado);
        //  dao.find(clientePesquisado);
//          dao.delete(clienteSelecionado.getId(), Cliente.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }
    
      public void pesquisaCliente() {
        this.listaCliente = dao.find(this.clientePesquisado);

    }

}


