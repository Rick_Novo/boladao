/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.ProdutoDAO;
import br.com.boladao.modelos.Produto;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Daniel
 */
@ManagedBean(name = "ProdutoBean")
public class ProdutoBean extends Bean {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");

    private final ProdutoDAO dao = new ProdutoDAO();
    private Produto produto = new Produto();

    public List<Produto> getListaProduto() {
        return listaProduto;
    }

    public void setListaProduto(List<Produto> listaProduto) {
        this.listaProduto = listaProduto;
    }

    private List<Produto> listaProduto;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(produto);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso!");
            this.produto = new Produto();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(produto.getId(), Produto.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public String editar() {

        System.out.println("a");

        return null;

    }

    public List<Produto> getProdutos() {
        List<Produto> lista;

        lista = dao.findAll();

        return lista;
    }

    public List<Produto> encontreiro(String a) {
        List<Produto> lista;

        lista = dao.find(a);

        return lista;
    }

}
