/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.FornecedorDAO;
import br.com.boladao.modelos.Fornecedor;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author sala303b
 */
@ManagedBean
@ViewScoped
public class PesquisaFornecedorBean extends Bean {

    private final FornecedorDAO dao = new FornecedorDAO();

    private Fornecedor fornecedorSelecionado = new Fornecedor();
    private Fornecedor fornecedorPesquisado = new Fornecedor();

    public PesquisaFornecedorBean() {

    }

    private List<Fornecedor> listaFornecedor;

    public List<Fornecedor> getListaFornecedor() {
        return listaFornecedor;
    }

    public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
        this.listaFornecedor = listaFornecedor;
    }

    public Fornecedor getFornecedorSelecionado() {
        return fornecedorSelecionado;
    }

    public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
        this.fornecedorSelecionado = fornecedorSelecionado;
    }

    public Fornecedor getFornecedorPesquisado() {
        return fornecedorPesquisado;
    }

    public void setFornecedorPesquisado(Fornecedor fornecedorPesquisado) {
        this.fornecedorPesquisado = fornecedorPesquisado;
    }

   

    public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(this.fornecedorSelecionado);
            dao.commitAndCloseTransaction();
            addMessageAviso("Alterado com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
        return null;

    }

    public void remover() {

        try {
            dao.beginTransaction();
            this.fornecedorSelecionado.setAtivo(false);
            dao.update(this.fornecedorSelecionado);
            dao.commitAndCloseTransaction();
            addMessageAviso("Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    
    
     public void pesquisaFornecedores() {
       this.listaFornecedor = dao.find(this.fornecedorPesquisado);
       
    }
    
}
