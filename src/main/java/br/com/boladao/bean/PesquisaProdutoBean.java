/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.ProdutoDAO;
import br.com.boladao.modelos.Produto;

import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author sala303b
 */
@ManagedBean
public class PesquisaProdutoBean {

    private final ProdutoDAO dao = new ProdutoDAO();

    private String nome;
   

    private List<Produto> listaProduto;

    public PesquisaProdutoBean() {
        
        this.listaProduto = this.retornaTodosProdutos();
         
              
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

  

    public List<Produto> getListaProduto() {
        return listaProduto;
    }

    public void setListaProduto(List<Produto> listaProduto) {
        this.listaProduto = listaProduto;
    }

    public void pesquisaProdutos() {
        
        this.listaProduto = dao.find(this.nome);
    }
    
    public List<Produto> retornaTodosProdutos() {
        
        this.listaProduto = dao.ListaTodosProdutos();
        return listaProduto;
    }

}
