/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boladao.bean;

import br.com.boladao.DAO.ClienteDAO;
import br.com.boladao.DAO.VendaDAO;
import br.com.boladao.modelos.Cliente;
import br.com.boladao.modelos.ItemDeVenda;
import br.com.boladao.modelos.Produto;
import br.com.boladao.modelos.Venda;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author sala303b
 */
@ManagedBean(name = "VendaBean")
@SessionScoped
public class VendaBean extends Bean {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoladaoPU");

    private Produto produtoSelecionado;

    private final VendaDAO dao = new VendaDAO();

    private Venda venda = new Venda();

    private int quantidadeProduto;

    private float preco;

    public VendaBean() {

    }

    public void salvarItem() {

        try {
            dao.beginTransaction();
            dao.save(venda);
            dao.commitAndCloseTransaction();
            addMessageInfo("Salvo com Sucesso !");
            this.venda = new Venda();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

    }

    public Cliente getCliente() {
        return this.venda.getCliente();
    }

    public void setCliente(Cliente cliente) {
        this.venda.setCliente(cliente);
    }

    private boolean validarProduto() {

       
        if (quantidadeProduto == 0) {
            addMessageAviso("Favor selecionar a quantidade!");
            return false;
        }

        if (preco == 0) {
            addMessageAviso("Favor selecionar valor!");
            return false;
        }
        return true;
    }

    public void adicionarProduto() {

        if (this.validarProduto()) {
            ItemDeVenda item = new ItemDeVenda();
            item.setProduto(produtoSelecionado);
            item.setPreco(preco);
            item.setQuantidade(quantidadeProduto);

            this.venda.getListaProdutos().add(item);

            this.produtoSelecionado = new Produto();

            this.quantidadeProduto = 0;
            this.preco = 0;
        }

//        try{
//        dao.beginTransaction();
//            dao.save(venda);
//            dao.commitAndCloseTransaction();
//            addMessageInfo("Salvo com Sucesso!");
//            this.venda = new Venda();
//        } catch (Exception ex) {
//            dao.rollback();
//            addMessageErro(ex.getMessage());
//        }
    }

    public List<ItemDeVenda> getItems() {
        return this.venda.getListaProdutos();

    }

    public int getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(int quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public void atualizarPreco(SelectEvent e) {

        this.preco = this.produtoSelecionado.getPreco();

    }

    /* public List<Cliente> getClientes() {
     List<Cliente> lista;
     dao.beginTransaction();
     lista = dao.findAll();
     dao.closeTransaction();
     return lista;
     } */
    public void salvar() {

        try {
            dao.salva(venda);
            addMessageInfo("Venda No." + venda.getId() + " Salva com Sucesso !");
            this.venda = new Venda();
        } catch (RuntimeException re) {
            re.printStackTrace();
            dao.rollback();
            throw re;
        } catch (Exception ex) {
            ex.printStackTrace();
            addMessageErro(ex.getMessage());
        }
    }

    public int getMaxSpiner() {
        return quantidadeProduto;
    }

    public void setMaxSpiner(int maxSpiner) {
        this.quantidadeProduto = maxSpiner;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

}
